﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DemoTestResults;

namespace Math_UnitTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestAddSuccess()
        {
            MyMath math = new MyMath();

            Assert.IsTrue(math.Add(1, 2) == 3);
        }

        [TestMethod]
        public void TestAddFail()
        {
            MyMath math = new MyMath();

            Assert.IsTrue(math.Add(1, 2) != 3);
        }

        [TestMethod]
        public void TestMultiplySuccess()
        {
            MyMath math = new MyMath();

            Assert.IsTrue(math.Multiply(1, 2) == 2);
        }

        [TestMethod]
        public void TestMultiplyFail()
        {
            MyMath math = new MyMath();

            Assert.IsTrue(math.Multiply(1, 2) != 2);
        }

        [TestMethod]
        public void TestSubtractFail()
        {
            MyMath math = new MyMath();

            Assert.IsTrue(math.Multiply(2, 1) == 1);
        }

        [TestMethod]
        public void TestSubtractSuccess()
        {
            MyMath math = new MyMath();

            Assert.IsTrue(math.Multiply(2, 1) != 1);
        }

        [TestMethod]
        public void TestDivideSuccess()
        {
            MyMath math = new MyMath();

            Assert.IsTrue(math.Divide(2, 1) == 2);
        }

        [TestMethod]
        public void TestDivideFail()
        {
            MyMath math = new MyMath();

            Assert.IsTrue(math.Divide(2, 1) != 2);
        }


    }
}
